import buildRoute from '../../utils/buildRoute';
import ChatRoomController from './ChatRoom.Controller';

const ChatRoomRoute = buildRoute(ChatRoomController);

export default ChatRoomRoute;
