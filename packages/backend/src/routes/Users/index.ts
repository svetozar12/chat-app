import buildRoute from '../../utils/buildRoute';
import UsersController from './UsersController';

const UsersRoute = buildRoute(UsersController);

export default UsersRoute;
