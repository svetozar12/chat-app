import buildRoute from '../../utils/buildRoute';
import MessagesController from './Messages.Controller';

const MessagesRoute = buildRoute(MessagesController);

export default MessagesRoute;
