import buildRoute from '../../utils/buildRoute';
import InvitesController from './Invites.Controller';

const InvitesRoute = buildRoute(InvitesController);

export default InvitesRoute;
