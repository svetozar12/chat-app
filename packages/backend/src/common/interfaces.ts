export enum Status {
  ACCEPTED = 'accepted',
  RECIEVED = 'recieved',
  DECLINED = 'declined',
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  OTHERS = 'others',
}
