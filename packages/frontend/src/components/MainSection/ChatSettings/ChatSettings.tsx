import { css, cx } from '@emotion/css';
import React, { FC } from 'react';
import { useRouter } from 'next/router';
import { AiOutlineUserDelete, AiOutlinePlusCircle } from 'react-icons/ai';
import { useCookie } from 'next-cookie';
import { Heading, HStack, VStack } from '@chakra-ui/react';
// services
import s from './ChatSettings.module.css';
import useThemeColors from '../../../hooks/useThemeColors';
import { STATE } from 'services/redux/reducer';
import { bindActionCreators, Dispatch } from 'redux';
import { toggleInviteModal } from 'services/redux/reducer/toggles/actions';
import { connect } from 'react-redux';
import { IWebSocket } from 'services/redux/reducer/websocket/state';
import { IToggle } from 'services/redux/reducer/toggles/state';
import { useGetChatListQuery, useGetChatQuery, useUpdateChatMutation } from 'services/generated';
import useProvideAuth from 'hooks/useSession';

interface IChatSettings {
  chatId: string;
  ws: IWebSocket;
  toggle: IToggle;
  toggleInviteModal: typeof toggleInviteModal;
}

const ChatSettings: FC<IChatSettings> = (props) => {
  const { chatId, ws, toggle } = props;
  const [users, setUsers] = React.useState<string[]>([]);
  const route = useRouter();
  const { auth } = useProvideAuth();
  const [updateChat] = useUpdateChatMutation();
  const { data } = useGetChatQuery({ variables: { auth, chat_id: chatId } });
  const { data: chatListData } = useGetChatListQuery({ variables: { auth } });

  const emitFriendRequest = async () => {
    ws.ws?.emit('friend_request');
  };
  const getMembers = async () => {
    try {
      const { getChatById } = data || {};
      if (getChatById?.__typename === 'Error') throw new Error(getChatById.message);
      if (!getChatById) return;
      const { members } = getChatById;
      setUsers(members);
      return true;
    } catch (error) {
      return false;
    }
  };

  const deleteMember = async (user: string) => {
    try {
      await updateChat({ variables: { auth, chat_id: chatId, username: user } });
      return true;
    } catch (error) {
      return false;
    }
  };

  React.useEffect(() => {
    setUsers([]);
    getMembers();
    ws.ws?.on('inviting_multiple_users', ({ users }) => {
      setUsers((prev) => [...prev, ...users]);
    });
  }, [route.asPath]);

  const redirect = async (user: string) => {
    const updatedUsers = users.filter((element) => element !== user);
    setUsers(updatedUsers);
    if (updatedUsers.length === 2) {
      const { getAllChats } = chatListData || {};
      if (getAllChats?.__typename === 'Error') throw new Error(getAllChats.message);
      const firstChatid = getAllChats?.res[0]._id;
      route.push(`/${firstChatid}`);
    }
  };

  const {
    base: {
      default: { color },
    },
  } = useThemeColors();

  return (
    <VStack mt={5} gap={5} pos="relative" transition="ease" w="full" opacity={toggle.toggleChatSettings ? 1 : 0}>
      <Heading w="70%" color={color} textAlign="center" whiteSpace="nowrap" fontSize={{ base: '4vw', md: '2vw' }}>
        Members in chat
      </Heading>

      {users?.map((item, index) => (
        <HStack alignItems="center" key={index}>
          <Heading color={color}>{item}</Heading>
          <AiOutlineUserDelete
            style={{ color }}
            onClick={() => {
              deleteMember(item);
              emitFriendRequest();
              redirect(item);
            }}
            className={s.removeUser}
          />
        </HStack>
      ))}
      {users.length > 2 && (
        <div
          onClick={() => toggleInviteModal(!toggle.toggleInvideModal)}
          className={cx(
            'flex',
            css`
              position: relative;
              z-index: 101;
              width: 2rem;
              height: 2rem;
              cursor: pointer;
              padding: 2rem 0.5rem;
              border-radius: 5px;
              justify-content: space-between;
              width: 70%;
              height: 2rem;
              whitespace: nowrap;
              &:hover {
                background: rgba(0, 0, 0, 0.1);
              }
            `,
          )}
        >
          <h2
            className={css`
              color: var(--main-black);
              margin: 0;
              @media (max-width: 1344px) {
                font-size: 1.5vw;
                word-break: keep-all;
              }
            `}
          >
            Add more users
          </h2>
          <div className="flex">
            <AiOutlinePlusCircle
              className={css`
                width: 2rem;
                height: 2rem;
              `}
            />
          </div>
        </div>
      )}
    </VStack>
  );
};

const mapStateToProps = (state: STATE) => ({
  ws: state.ws,
  toggle: state.toggle,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  toggleInviteModal: bindActionCreators(toggleInviteModal, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatSettings);
