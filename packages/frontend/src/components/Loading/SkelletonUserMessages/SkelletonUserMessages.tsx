import React from 'react';
import { Skeleton } from '@chakra-ui/react';

function SkelletonUserSettings() {
  return (
    <Skeleton w="full" h="100vh" pos="relative" zIndex="10">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi nostrum doloribus tenetur maxime optio, provident quia ullam deleniti
      itaque molestias temporibus enim quo adipisci tempora quam dolore error quas necessitatibus.
    </Skeleton>
  );
}

export default SkelletonUserSettings;
