import { Socket } from 'socket.io-client';

export interface IWebSocket {
  ws: null | Socket;
}
