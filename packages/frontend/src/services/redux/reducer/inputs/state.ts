export default interface IInputs {
  input_username: string;
  input_password: string;
  input_email: string;
  input_gender: 'male' | 'female' | 'other';
  input_file: any;
}
