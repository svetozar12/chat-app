export interface IAuth {
  remember_me: boolean;
  loginPrompt: boolean;
  isAuth: boolean;
}
