export interface IToggle {
  toggleFriendReqModal: boolean;
  toggleCreateGroupModal: boolean;
  toggleMobileNav: boolean;
  toggleIsMatch: boolean;
  toggleChatSettings: boolean;
  toggleInvideModal: boolean;
  toggleIsLoggedIn: boolean;
  toggeleIsLoading: boolean;
  toggleQuickLogin: boolean;
}
