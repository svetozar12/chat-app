import { AlertStatus } from '@chakra-ui/react';

export interface IAlert {
  message: string;
  type: AlertStatus;
}
