const jwtTokens = {
  access_short_expiration: 3600,
  access_long_expiration: 7200,
  refresh_short_expiration: 86400,
  refresh_long_expiration: 2.592e6,
};

export default jwtTokens;
